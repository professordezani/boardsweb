
public class BoardMemoryRepository : IBoardRepository
{
    private List<Board> boards = new List<Board>();

    public void Create(Board board)
    {
        board.BoardId = Guid.NewGuid();
        boards.Add(board);
    }

    public void Delete(Guid id)
    {
        throw new NotImplementedException();
    }

    public IEnumerable<Board> Read()
    {
        return boards;
    }

    public Board Read(Guid id)
    {
        throw new NotImplementedException();
    }

    public void Update(Guid id, Board board)
    {
        throw new NotImplementedException();
    }
}