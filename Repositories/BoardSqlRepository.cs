
using Microsoft.Data.SqlClient;

public class BoardSqlRepository : DatabaseConnection, IBoardRepository
{
    public void Create(Board board)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = connection;
        cmd.CommandText = "INSERT INTO Board VALUES (NEWID(), @name)";

        cmd.Parameters.AddWithValue("@name", board.Name);

        cmd.ExecuteNonQuery();
    }

    public void Delete(Guid id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = connection;
        cmd.CommandText = "DELETE FROM Board WHERE BoardId = @id";

        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();
    }

    public IEnumerable<Board> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = connection;
        cmd.CommandText = "SELECT * FROM Board";

        SqlDataReader reader = cmd.ExecuteReader();

        List<Board> boards = new List<Board>();

        while(reader.Read())
        {
            Board board = new Board();
            board.BoardId = reader.GetGuid(0);
            board.Name = reader.GetString(1);

            boards.Add(board);
        }

        return boards;
    }

    public Board Read(Guid id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = connection;
        cmd.CommandText = "SELECT * FROM Board WHERE BoardId = @id";

        cmd.Parameters.AddWithValue("@id", id);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read())
        {
            Board board = new Board();
            board.BoardId = reader.GetGuid(0);
            board.Name = reader.GetString(1);

            return board;
        }

        return null;
    }

    public void Update(Guid id, Board board)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = connection;
        cmd.CommandText = "UPDATE Board SET Name = @name WHERE BoardId = @id";

        cmd.Parameters.AddWithValue("@id", id);
        cmd.Parameters.AddWithValue("@name", board.Name);

        cmd.ExecuteNonQuery();
    }
}