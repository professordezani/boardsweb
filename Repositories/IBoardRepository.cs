public interface IBoardRepository
{
    void Create(Board board);

    IEnumerable<Board> Read();

    Board Read(Guid id);

    void Update(Guid id, Board board);

    void Delete(Guid id);
}