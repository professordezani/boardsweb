using Microsoft.Data.SqlClient;

public abstract class DatabaseConnection : IDisposable
{
    protected SqlConnection connection;

    public DatabaseConnection() 
    {
        Console.WriteLine("Abriu conexão");
        string strConn = @"Data Source=localhost; 
        Initial Catalog=DBBoards; 
        Integrated Security=False;
        User=aluno;
        Password=dba;
        TrustServerCertificate=True";
        connection = new SqlConnection(strConn);
        connection.Open();
    }

    public void Dispose()
    {
        Console.WriteLine("Fechou conexão");
        connection.Close();
    }
}