using System.ComponentModel.DataAnnotations;

public class Interation
{
    // propriedades automáticas:
    public Guid InterationId { get; set; }

    public string Name { get; set; }

    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
    public DateTime StartDate { get; set; }

    [DataType(DataType.Date)]   
    [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")] 
    public DateTime EndDate { get; set; }
}