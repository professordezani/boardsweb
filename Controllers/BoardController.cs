using Microsoft.AspNetCore.Mvc;

public class BoardController : Controller
{
    private readonly IBoardRepository repository;

    public BoardController(IBoardRepository repository)
    {
        this.repository = repository;
    }

    // GET http://localhost:123/board/index
    public ActionResult Index()
    {
        // return /Views/Board/Index.cshtml
        IEnumerable<Board> boards = repository.Read();

        return View(boards);
    }

    // GET http://localhost:123/board/create
    [HttpGet]
    public ActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Create(Board board)
    {
        repository.Create(board);

        return RedirectToAction("Index");
    }

    [HttpGet]
    public ActionResult Update(Guid id)
    {        
        return View(repository.Read(id));
    }

    [HttpPost]
    public ActionResult Update(Guid id, Board board)
    {
        repository.Update(id, board);

        return RedirectToAction("Index");
    }

    [HttpGet]
    public ActionResult Delete(Guid id)
    {        
        repository.Delete(id);
        return RedirectToAction("Index");
    }

}