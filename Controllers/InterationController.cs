using Microsoft.AspNetCore.Mvc;

public class InterationController : Controller
{
    private readonly IInterationRepository repository;

    public InterationController(IInterationRepository repository)
    {
        this.repository = repository;
    }

    public ActionResult Index()
    {
        IEnumerable<Interation> interations = repository.Read();
        return View(interations);
    }

    [HttpGet]
    public ActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Create(Interation interation)
    {
        repository.Create(interation);

        return RedirectToAction("Index");
    }

    [HttpGet]
    public ActionResult Update(Guid id)
    {
        Interation interation = repository.Read(id);
        return View(interation);
    }

    [HttpPost]
    public ActionResult Update(Guid id, Interation interation)
    {
        repository.Update(id, interation);
        return RedirectToAction("Index");
    }

    [HttpGet]
    public ActionResult Delete(Guid id)
    {
        repository.Delete(id);
        return RedirectToAction("Index");
    }

}