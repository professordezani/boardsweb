create database DBBoards
go

use DBBoards
go

create table Board
(
	BoardId		uniqueidentifier	primary key,
	Name		varchar(max)		not null
)

create table Interation
(
	InterationId	uniqueidentifier	primary key,
	BoardId			uniqueidentifier	not null,
	Name			varchar(max)		not null,
	StartDate		datetime			not null,
	EndDate			datetime			not null,
	constraint FK_BoardInteration 
		foreign key (BoardId) references Board(BoardId)
)


insert into Board values (NEWID(), 'Board 1')

insert into Interation 
	values (NEWID(),
	'DDA2B0D0-63F7-4262-95B1-F8A24964BE5D',
	'Interation 1',
	GETDATE(),
	GETDATE() + 1)

select * from Board

select * from Interation

select * 
from Interation
INNER JOIN Board ON Interation.BoardId = Board.BoardId

create view interations as
	select InterationId,
			Board.BoardId,
			Interation.Name,
			StartDate,
			EndDate,
			Board.Name as BoardName
	from Interation
	INNER JOIN Board ON Interation.BoardId = Board.BoardId

select * from interations